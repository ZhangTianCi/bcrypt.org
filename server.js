// --------------------------------------------------------------------------------------------------------------------

"use strict"

// core
const http = require('http')

// local
const env = require('./lib/env.js')
const log = require('./lib/log.js')
const app = require('./lib/app.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

process.title = env.apex

// every so often, print memory usage
var memUsageEverySecs = env.isProd ? 10 * 60 : 30
setInterval(() => {
  log.withFields(process.memoryUsage()).debug('memory')
}, memUsageEverySecs * 1000)

// --------------------------------------------------------------------------------------------------------------------
// server

const server = http.createServer()
server.on('request', app)

server.listen(env.port, function() {
  log.withFields({ port : env.port }).info('server-started')
})

process.on('SIGTERM', () => {
  log.info('sigterm')
  server.close(() => {
    log.info('exiting')
    process.exit(0)
  })
})

// --------------------------------------------------------------------------------------------------------------------
