// --------------------------------------------------------------------------------------------------------------------

// core
const fs = require('fs')
const os = require('os')

// npm
const express = require('express')
const compress = require('compression')
const favicon = require('serve-favicon')
const bodyParser = require('body-parser')
const yid = require('yid')
const LogFmtr = require('logfmtr')

// local
const pkg = require('../package.json')
const log = require('./log.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

const isProd = process.env.NODE_ENV === 'production'
const protocol = 'https'
const nakedDomain = pkg.name
const baseUrl = protocol + '://' + nakedDomain

// create the sitemap
const sitemap = [
  baseUrl + '/',
]
const sitemapTxt = sitemap.join('\n') + '\n'

// --------------------------------------------------------------------------------------------------------------------
// application server

const app = express()
app.set('case sensitive routing', true)
app.set('strict routing', true)
app.set('views', __dirname + '/../views')
app.set('view engine', 'pug')
app.enable('trust proxy')

app.locals.pkg = pkg
app.locals.env = process.env.NODE_ENV
app.locals.min = isProd ? '.min' : ''
app.locals.pretty = isProd

// do all static routes first
app.use(compress())
app.use(favicon(__dirname + '/../public/favicon.ico'))

if ( isProd ) {
  const oneMonth = 30 * 24 * 60 * 60 * 1000
  app.use(express.static(__dirname + '/../public/', { maxAge : oneMonth }))
}
else {
  app.use(express.static(__dirname + '/../public/'))
}

app.use(bodyParser.urlencoded({
  extended : false,
  limit    : '1mb',
}))

// general middleware
require('./middleware.js')(app)

// routes
require('./routes/home.js')(app)
require('./routes/api.js')(app)

// --------------------------------------------------------------------------------------------------------------------
// export the app

module.exports = app

// --------------------------------------------------------------------------------------------------------------------
