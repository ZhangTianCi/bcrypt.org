// --------------------------------------------------------------------------------------------------------------------

// local
const env = require('../env.js')

// --------------------------------------------------------------------------------------------------------------------
// setup

// create the sitemap
const sitemap = [
  env.baseUrl + '/',
]
const sitemapTxt = sitemap.join('\n') + '\n'

// --------------------------------------------------------------------------------------------------------------------

function routes(app) {

  app.get("/", (req, res) => {
    res.render('index')
  })

  app.get(
    '/sitemap.txt',
    (req, res) => {
      res.setHeader('Content-Type', 'text/plain')
      res.send(sitemapTxt)
    }
  )

  app.get(
    '/uptime',
    (req, res) => {
      res.setHeader('Content-Type', 'text/plain')
      res.send(String(process.uptime()))
    }
  )

}

// --------------------------------------------------------------------------------------------------------------------

module.exports = routes

// --------------------------------------------------------------------------------------------------------------------
