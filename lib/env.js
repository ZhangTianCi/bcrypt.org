// --------------------------------------------------------------------------------------------------------------------

// local
const pkg = require('../package.json')

// --------------------------------------------------------------------------------------------------------------------

const protocol = 'https'
const apex = pkg.name
const baseUrl = protocol + '://' + apex

const port = process.env.PORT || 3000
const isProd = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod'

module.exports = {
  protocol,
  apex,
  baseUrl,
  port,
  isProd,
}

// --------------------------------------------------------------------------------------------------------------------
